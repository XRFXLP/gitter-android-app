package im.gitter.gitter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import im.gitter.gitter.models.Room;
import im.gitter.gitter.network.VolleySingleton;
import im.gitter.gitter.utils.AvatarUtils;
import im.gitter.gitter.utils.CursorUtils;

public class CommunityRoomListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int MEMBER_HEADING_INDEX = -1;
    private static final int NON_MEMBER_HEADING_INDEX = -2;

    private static final int ROOM_VIEW_TYPE = 0;
    private static final int HEADING_VIEW_TYPE = 1;

    private final ImageLoader imageLoader;
    private final AvatarUtils avatarUtils;

    private Cursor cursor;
    private List<Room> rooms = new ArrayList<>();
    private Integer firstMemberRoom = null;
    private Integer firstNonMemberRoom = null;

    public CommunityRoomListAdapter(Context context) {
        imageLoader = VolleySingleton.getInstance(context).getImageLoader();
        avatarUtils = new AvatarUtils(context);
        setHasStableIds(true);
    }

    @Override
    public int getItemCount() {
        int count = rooms.size();
        if (firstMemberRoom != null) {
            count += 1;
        }
        if (firstNonMemberRoom != null) {
            count += 1;
        }
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        int index = mapPositionToRoomIndex(position);
        if (index == MEMBER_HEADING_INDEX || index == NON_MEMBER_HEADING_INDEX) {
            return HEADING_VIEW_TYPE;
        } else {
            return ROOM_VIEW_TYPE;
        }
    }

    @Override
    public long getItemId(int position) {
        int index = mapPositionToRoomIndex(position);
        if (index == MEMBER_HEADING_INDEX || index == NON_MEMBER_HEADING_INDEX) {
            return RecyclerView.NO_ID;
        } else {
            return rooms.get(index).getId().hashCode();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADING_VIEW_TYPE) {
            View headingView = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_list_heading, parent, false);
            return new HeadingViewHolder(headingView);
        } else {
            View roomView = LayoutInflater.from(parent.getContext()).inflate(R.layout.avatar_with_badge, parent, false);
            return new RoomViewHolder(roomView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int index = mapPositionToRoomIndex(position);
        if (index == MEMBER_HEADING_INDEX) {
            HeadingViewHolder headingViewHolder = (HeadingViewHolder) holder;
            headingViewHolder.textView.setText("My Rooms");
        } else if (index == NON_MEMBER_HEADING_INDEX) {
            HeadingViewHolder headingViewHolder = (HeadingViewHolder) holder;
            headingViewHolder.textView.setText("Other Rooms");
        } else {
            RoomViewHolder roomViewHolder = (RoomViewHolder) holder;
            Room room = rooms.get(index);
            roomViewHolder.avatarView.setImageUrl(avatarUtils.getAvatarWithDimen(room.getAvatarUrl(), R.dimen.avatar_with_badge_avatar_size), imageLoader);
            roomViewHolder.avatarView.setDefaultImageResId(R.drawable.default_avatar);
            roomViewHolder.titleView.setText(room.getName());
            roomViewHolder.badgeView.setBadge(room.getUnreadCount(), room.getMentionCount());
            roomViewHolder.roomId = room.getId();
        }
    }

    public void setCursor(Cursor cursor) {
        if (this.cursor != cursor) {
            this.cursor = cursor;
            rooms = new ArrayList<>();
            firstMemberRoom = null;
            firstNonMemberRoom = null;

            if (cursor != null) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    int position = cursor.getPosition();
                    Room room = Room.newInstance(CursorUtils.getContentValues(cursor));

                    if (room.isRoomMember()) {
                        if (firstMemberRoom == null || position < firstMemberRoom) {
                            firstMemberRoom = position;
                        }
                    } else {
                        if (firstNonMemberRoom == null || position < firstNonMemberRoom) {
                            firstNonMemberRoom = position;
                        }
                    }
                    rooms.add(room);
                }
            }
            notifyDataSetChanged();
        }
    }

    private int mapPositionToRoomIndex(int position) {
        int index = position;
        if (firstMemberRoom != null) {
            if (index == firstMemberRoom) {
                return MEMBER_HEADING_INDEX;
            } else if (firstMemberRoom < index) {
                index -= 1;
            }
        }
        if (firstNonMemberRoom != null) {
            if (index == firstNonMemberRoom) {
                return NON_MEMBER_HEADING_INDEX;
            } else if (firstNonMemberRoom < index) {
                index -= 1;
            }
        }

        return index;
    }

    private static class HeadingViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        HeadingViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.heading_text);
        }
    }
}
